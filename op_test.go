package sqlb

import (
	"reflect"
	"testing"
)

type opResult struct {
	c    Sqler
	sql  string
	data interface{}
}

func TestOps(t *testing.T) {
	cases := []opResult{
		//EQ
		opResult{
			c:    Eq{"baz", "quz"},
			sql:  "baz=?",
			data: []interface{}{"quz"},
		},
		opResult{
			c:    Eq{"baz", 42},
			sql:  "baz=?",
			data: []interface{}{42},
		},

		//NEQ
		opResult{
			c:    Neq{"baz", "quz"},
			sql:  "baz!=?",
			data: []interface{}{"quz"},
		},
		opResult{
			c:    Neq{"baz", 42},
			sql:  "baz!=?",
			data: []interface{}{42},
		},

		//LT
		opResult{
			c:    Lt{"baz", "quz"},
			sql:  "baz<?",
			data: []interface{}{"quz"},
		},
		opResult{
			c:    Lt{"baz", 42},
			sql:  "baz<?",
			data: []interface{}{42},
		},

		//LTE
		opResult{
			c:    Lte{"baz", "quz"},
			sql:  "baz<=?",
			data: []interface{}{"quz"},
		},
		opResult{
			c:    Lte{"baz", 42},
			sql:  "baz<=?",
			data: []interface{}{42},
		},

		//GT
		opResult{
			c:    Gt{"baz", "quz"},
			sql:  "baz>?",
			data: []interface{}{"quz"},
		},
		opResult{
			c:    Gt{"baz", 42},
			sql:  "baz>?",
			data: []interface{}{42},
		},

		//GT
		opResult{
			c:    Gte{"baz", "quz"},
			sql:  "baz>=?",
			data: []interface{}{"quz"},
		},
		opResult{
			c:    Gte{"baz", 42},
			sql:  "baz>=?",
			data: []interface{}{42},
		},
	}

	for _, c := range cases {
		sql, data := c.c.Sql()
		if sql != c.sql {
			t.Errorf("Expected %s, got %s", c.sql, sql)
		}
		if !reflect.DeepEqual(data, c.data) {
			t.Errorf("Expected %#v, got %#v", c.data, data)
		}
	}
}
