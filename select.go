package sqlb

import "strings"

type SelectQuery struct {
	table string
	where []Sqler
}

func (s *SelectQuery) Where(clauses ...Sqler) *SelectQuery {
	s.where = append(s.where, clauses...)
	return s
}

func (s *SelectQuery) Sql() (string, []interface{}) {
	sql := "SELECT * FROM " + s.table + " "
	data := []interface{}{}

	clauses := []string{}
	for _, w := range s.where {
		thisSql, thisData := w.Sql()
		clauses = append(clauses, thisSql)
		data = append(data, thisData...)

	}
	if len(clauses) > 0 {
		sql = sql + "WHERE " + strings.Join(clauses, " AND ")
	}

	return sql, data
}
