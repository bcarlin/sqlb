package sqlb

type TableNamer interface {
	TableName() string
}

type Sqler interface {
	Sql() (string, []interface{})
}

type SqlQuery struct {
	table string
}

func (s *SqlQuery) Select() *SelectQuery {
	return &SelectQuery{table: s.table}
}

func Query(i TableNamer) *SqlQuery {
	return &SqlQuery{table: i.TableName()}
}

type Foo struct{}

func (f *Foo) TableName() string {
	return "foo"
}
