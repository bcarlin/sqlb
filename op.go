package sqlb

type Eq struct {
	field string
	val   interface{}
}

func (e Eq) Sql() (string, []interface{}) {
	return e.field + "=?", []interface{}{e.val}
}

type Neq struct {
	field string
	val   interface{}
}

func (n Neq) Sql() (string, []interface{}) {
	return n.field + "!=?", []interface{}{n.val}
}

type Lt struct {
	field string
	val   interface{}
}

func (l Lt) Sql() (string, []interface{}) {
	return l.field + "<?", []interface{}{l.val}
}

type Lte struct {
	field string
	val   interface{}
}

func (l Lte) Sql() (string, []interface{}) {
	return l.field + "<=?", []interface{}{l.val}
}

type Gt struct {
	field string
	val   interface{}
}

func (g Gt) Sql() (string, []interface{}) {
	return g.field + ">?", []interface{}{g.val}
}

type Gte struct {
	field string
	val   interface{}
}

func (g Gte) Sql() (string, []interface{}) {
	return g.field + ">=?", []interface{}{g.val}
}
